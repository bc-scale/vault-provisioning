resource "vault_mount" "ssh-host-signer" {
  type = "ssh"
  path = "ssh-host-signer"
}

resource "vault_ssh_secret_backend_ca" "ssh-host-signer" {
  backend = vault_mount.ssh-host-signer.path

  generate_signing_key = false
  public_key           = var.public_key
  private_key          = var.private_key
}

resource "vault_ssh_secret_backend_role" "host" {
  name                    = "host"
  backend                 = vault_mount.ssh-host-signer.path
  key_type                = "ca"
  allowed_domains         = var.allowed_domains
  allow_subdomains        = true
  allow_host_certificates = true
  ttl                     = "315360000"
}
